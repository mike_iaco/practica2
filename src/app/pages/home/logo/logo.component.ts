import { Component, Input, OnInit } from '@angular/core';

import { Logo } from '../models/Ihome';
@Component({
  selector: 'app-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.scss']
})
export class LogoComponent implements OnInit {
@Input() public logo: Logo;
  constructor() { }

  ngOnInit(): void {
  }

}
