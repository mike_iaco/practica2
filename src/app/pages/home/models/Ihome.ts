export interface Logo {
    img: string;
    text: string;
    description: string;
}

export interface Goals {
    title: string;
    text: string;
}

export interface Form {
    
}