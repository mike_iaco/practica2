import { Component, OnInit } from '@angular/core';
import { Logo, Goals, Form } from './models/Ihome';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  public logo: Logo[];
  public goals: Goals;
  public form: Form;
  constructor() {
    this.logo = [
      {
        img:
          'https://extrulub.com/wp-content/uploads/2020/04/Helix-HX71-10W40-300x300.png',
        text: 'Aceite Shell 10w40 Semisintetico',
        description: 'Aceite de motor de tecnología sintética: ayuda a que el motor se mantenga limpio y funcione con eficiencia.',
      },
      {
        img:
          'https://lh3.googleusercontent.com/proxy/m9EnOVQP1Rj7Is8LUT_42LP61GmnvypVJE7_Oni8ydD59ZOMg6EKOJBCXjjaIEaDH7kyhCqh102V9D2esdSh57VhzH6otSKzLDbhskbo0QM2WijlPiRoh3JpfAimS-9M9w6zBQqBnMtohQ',
        text: 'Aceite Shell 20w50 Mineral',
        description: 'Ha sido formulado con tecnología especial de limpieza activa que trabaja para prevenir la formación de depósitos de lodo o barro en el motor.',
      },
      {
        img:
          'https://lh3.googleusercontent.com/proxy/Md-9Q38o377RCtWHhpZ_fag286TI2AbxIY1kB5o-6xARya6bQJkv5BteZlWkKXu1zOvqSAAC7VWTZ3t-Xk2HZxt76w',
        text: 'Aceite Shell 5w30',
        description: ' Es un aceite con un contenido reducido de cenizas sulfatadas, azufre y fósforo por lo que es compatible con sistemas anticontaminación, como filtros de partículas y catalizadores.',
      },
      {
        img:
          'https://aceitesya.com/wp-content/uploads/2020/09/shell-80w90-324x324.png',
        text: 'Aceite Shell 80w90 GL-4',
        description: 'Todas las aplicaciones normalmente lubricadas con aceite para engranajes automotrices, como cojinetes de ruedas traseras, engranajes de dirección manual y juntas universales que requieran grado API GL-5, SAE 80W90.',
      },
    ];

    this.goals = {
      title: 'Nuestros Objetivos',
      text: 'Shell es una empresa energética integrada que tiene el objetivo de satisfacer la creciente demanda energética mundial de manera responsable desde los puntos de vista económico, medioambiental y social. Nuestra estrategia está orientada a reforzar nuestra posición como líder en el sector del petróleo y el gas, y a la vez satisfacer la creciente demanda energética mundial de una manera responsable. La seguridad, el respeto al medio ambiente y la responsabilidad social están en el centro de todas nuestras actividades.',
      
    };
    this.form = {};
  }

  ngOnInit(): void {}
}
