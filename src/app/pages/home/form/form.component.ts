import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
signupForm: FormGroup

  constructor(
    private _builder: FormBuilder
  ) { this.signupForm = this._builder.group({
    nombre: [''],
    apellido: ['',  Validators.required],
    email: ['', Validators.compose([Validators.email, Validators.required])],
    pedido: ['', Validators.required]

  })}

  enviar(values) {
    console.log(values)
  }
  ngOnInit(): void {
  }

}
